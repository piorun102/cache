PHONE: compile-proto

compile-proto:
	# Удаляем все указанные файлы в директории output
	rm -fr protobuf/*.pb.go
	export PATH="$PATH:$GOPATH/bin"
	protoc -I. shared/protobuf/*.proto validator/protobuf/*.proto processing/protobuf/*.proto handler/protobuf/*.proto --go_out=protobuf  --experimental_allow_proto3_optional
	mv protobuf/google.golang.org/protobuf/* protobuf
	rm -rf protobuf/google.golang.org

compile-proto-win:
	Remove-Item -Path p-cache/*.pb.go -Force
	protoc -I. controller/*.proto transaction/*.proto *.proto stock/*.proto market/*.proto --go_out=p-cache --experimental_allow_proto3_optional
	move p-cache/google.golang.org/p-cache/* p-cache
	Remove-Item -Path p-cache/google.golang.org -Recurse -Force