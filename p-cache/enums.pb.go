// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.32.0
// 	protoc        v4.25.1
// source: enums.proto

package p_cache

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	_ "google.golang.org/protobuf/types/descriptorpb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Status int32

const (
	Status_NULL       Status = 0
	Status_CANCEL     Status = -1
	Status_ERROR      Status = -2
	Status_CLOSED     Status = -3
	Status_VALIDATING Status = 1
	Status_PENDING    Status = 2
	Status_ACCEPTED   Status = 3
	Status_APPROVED   Status = 4
	Status_SUCCESS    Status = 5
	Status_CREATED    Status = 6
	Status_PARTIAL    Status = 10
)

// Enum value maps for Status.
var (
	Status_name = map[int32]string{
		0:  "NULL",
		-1: "CANCEL",
		-2: "ERROR",
		-3: "CLOSED",
		1:  "VALIDATING",
		2:  "PENDING",
		3:  "ACCEPTED",
		4:  "APPROVED",
		5:  "SUCCESS",
		6:  "CREATED",
		10: "PARTIAL",
	}
	Status_value = map[string]int32{
		"NULL":       0,
		"CANCEL":     -1,
		"ERROR":      -2,
		"CLOSED":     -3,
		"VALIDATING": 1,
		"PENDING":    2,
		"ACCEPTED":   3,
		"APPROVED":   4,
		"SUCCESS":    5,
		"CREATED":    6,
		"PARTIAL":    10,
	}
)

func (x Status) Enum() *Status {
	p := new(Status)
	*p = x
	return p
}

func (x Status) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Status) Descriptor() protoreflect.EnumDescriptor {
	return file_enums_proto_enumTypes[0].Descriptor()
}

func (Status) Type() protoreflect.EnumType {
	return &file_enums_proto_enumTypes[0]
}

func (x Status) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Status.Descriptor instead.
func (Status) EnumDescriptor() ([]byte, []int) {
	return file_enums_proto_rawDescGZIP(), []int{0}
}

type Token int32

const (
	Token_T_NULL Token = 0
	Token_USDT   Token = 1110
	Token_RUB    Token = 2100
)

// Enum value maps for Token.
var (
	Token_name = map[int32]string{
		0:    "T_NULL",
		1110: "USDT",
		2100: "RUB",
	}
	Token_value = map[string]int32{
		"T_NULL": 0,
		"USDT":   1110,
		"RUB":    2100,
	}
)

func (x Token) Enum() *Token {
	p := new(Token)
	*p = x
	return p
}

func (x Token) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Token) Descriptor() protoreflect.EnumDescriptor {
	return file_enums_proto_enumTypes[1].Descriptor()
}

func (Token) Type() protoreflect.EnumType {
	return &file_enums_proto_enumTypes[1]
}

func (x Token) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Token.Descriptor instead.
func (Token) EnumDescriptor() ([]byte, []int) {
	return file_enums_proto_rawDescGZIP(), []int{1}
}

type Bank int32

const (
	Bank_B_NULL  Bank = 0
	Bank_SBERRUB Bank = 2101
	Bank_TCSBRUB Bank = 2102
	Bank_RFBRUB  Bank = 2104
	Bank_OPNBRUB Bank = 2105
	Bank_ACRUB   Bank = 2111
	Bank_SOVKRUB Bank = 2124
	Bank_MKBRUB  Bank = 2125
	Bank_TBRUB   Bank = 2126
	Bank_MTSRUB  Bank = 2127
	Bank_RSHRUB  Bank = 2128
	Bank_GPBRUB  Bank = 2129
	Bank_POSTRUB Bank = 2130
	Bank_YMRUB   Bank = 2132
	Bank_SBPRUB  Bank = 2133
)

// Enum value maps for Bank.
var (
	Bank_name = map[int32]string{
		0:    "B_NULL",
		2101: "SBERRUB",
		2102: "TCSBRUB",
		2104: "RFBRUB",
		2105: "OPNBRUB",
		2111: "ACRUB",
		2124: "SOVKRUB",
		2125: "MKBRUB",
		2126: "TBRUB",
		2127: "MTSRUB",
		2128: "RSHRUB",
		2129: "GPBRUB",
		2130: "POSTRUB",
		2132: "YMRUB",
		2133: "SBPRUB",
	}
	Bank_value = map[string]int32{
		"B_NULL":  0,
		"SBERRUB": 2101,
		"TCSBRUB": 2102,
		"RFBRUB":  2104,
		"OPNBRUB": 2105,
		"ACRUB":   2111,
		"SOVKRUB": 2124,
		"MKBRUB":  2125,
		"TBRUB":   2126,
		"MTSRUB":  2127,
		"RSHRUB":  2128,
		"GPBRUB":  2129,
		"POSTRUB": 2130,
		"YMRUB":   2132,
		"SBPRUB":  2133,
	}
)

func (x Bank) Enum() *Bank {
	p := new(Bank)
	*p = x
	return p
}

func (x Bank) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Bank) Descriptor() protoreflect.EnumDescriptor {
	return file_enums_proto_enumTypes[2].Descriptor()
}

func (Bank) Type() protoreflect.EnumType {
	return &file_enums_proto_enumTypes[2]
}

func (x Bank) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Bank.Descriptor instead.
func (Bank) EnumDescriptor() ([]byte, []int) {
	return file_enums_proto_rawDescGZIP(), []int{2}
}

var File_enums_proto protoreflect.FileDescriptor

var file_enums_proto_rawDesc = []byte{
	0x0a, 0x0b, 0x65, 0x6e, 0x75, 0x6d, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07, 0x70,
	0x5f, 0x63, 0x61, 0x63, 0x68, 0x65, 0x1a, 0x20, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74,
	0x6f, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2a, 0xb0, 0x01, 0x0a, 0x06, 0x53, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x12, 0x08, 0x0a, 0x04, 0x4e, 0x55, 0x4c, 0x4c, 0x10, 0x00, 0x12, 0x13, 0x0a,
	0x06, 0x43, 0x41, 0x4e, 0x43, 0x45, 0x4c, 0x10, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0x01, 0x12, 0x12, 0x0a, 0x05, 0x45, 0x52, 0x52, 0x4f, 0x52, 0x10, 0xfe, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0x01, 0x12, 0x13, 0x0a, 0x06, 0x43, 0x4c, 0x4f, 0x53, 0x45, 0x44,
	0x10, 0xfd, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x01, 0x12, 0x0e, 0x0a, 0x0a, 0x56,
	0x41, 0x4c, 0x49, 0x44, 0x41, 0x54, 0x49, 0x4e, 0x47, 0x10, 0x01, 0x12, 0x0b, 0x0a, 0x07, 0x50,
	0x45, 0x4e, 0x44, 0x49, 0x4e, 0x47, 0x10, 0x02, 0x12, 0x0c, 0x0a, 0x08, 0x41, 0x43, 0x43, 0x45,
	0x50, 0x54, 0x45, 0x44, 0x10, 0x03, 0x12, 0x0c, 0x0a, 0x08, 0x41, 0x50, 0x50, 0x52, 0x4f, 0x56,
	0x45, 0x44, 0x10, 0x04, 0x12, 0x0b, 0x0a, 0x07, 0x53, 0x55, 0x43, 0x43, 0x45, 0x53, 0x53, 0x10,
	0x05, 0x12, 0x0b, 0x0a, 0x07, 0x43, 0x52, 0x45, 0x41, 0x54, 0x45, 0x44, 0x10, 0x06, 0x12, 0x0b,
	0x0a, 0x07, 0x50, 0x41, 0x52, 0x54, 0x49, 0x41, 0x4c, 0x10, 0x0a, 0x2a, 0x28, 0x0a, 0x05, 0x54,
	0x6f, 0x6b, 0x65, 0x6e, 0x12, 0x0a, 0x0a, 0x06, 0x54, 0x5f, 0x4e, 0x55, 0x4c, 0x4c, 0x10, 0x00,
	0x12, 0x09, 0x0a, 0x04, 0x55, 0x53, 0x44, 0x54, 0x10, 0xd6, 0x08, 0x12, 0x08, 0x0a, 0x03, 0x52,
	0x55, 0x42, 0x10, 0xb4, 0x10, 0x2a, 0xca, 0x01, 0x0a, 0x04, 0x42, 0x61, 0x6e, 0x6b, 0x12, 0x0a,
	0x0a, 0x06, 0x42, 0x5f, 0x4e, 0x55, 0x4c, 0x4c, 0x10, 0x00, 0x12, 0x0c, 0x0a, 0x07, 0x53, 0x42,
	0x45, 0x52, 0x52, 0x55, 0x42, 0x10, 0xb5, 0x10, 0x12, 0x0c, 0x0a, 0x07, 0x54, 0x43, 0x53, 0x42,
	0x52, 0x55, 0x42, 0x10, 0xb6, 0x10, 0x12, 0x0b, 0x0a, 0x06, 0x52, 0x46, 0x42, 0x52, 0x55, 0x42,
	0x10, 0xb8, 0x10, 0x12, 0x0c, 0x0a, 0x07, 0x4f, 0x50, 0x4e, 0x42, 0x52, 0x55, 0x42, 0x10, 0xb9,
	0x10, 0x12, 0x0a, 0x0a, 0x05, 0x41, 0x43, 0x52, 0x55, 0x42, 0x10, 0xbf, 0x10, 0x12, 0x0c, 0x0a,
	0x07, 0x53, 0x4f, 0x56, 0x4b, 0x52, 0x55, 0x42, 0x10, 0xcc, 0x10, 0x12, 0x0b, 0x0a, 0x06, 0x4d,
	0x4b, 0x42, 0x52, 0x55, 0x42, 0x10, 0xcd, 0x10, 0x12, 0x0a, 0x0a, 0x05, 0x54, 0x42, 0x52, 0x55,
	0x42, 0x10, 0xce, 0x10, 0x12, 0x0b, 0x0a, 0x06, 0x4d, 0x54, 0x53, 0x52, 0x55, 0x42, 0x10, 0xcf,
	0x10, 0x12, 0x0b, 0x0a, 0x06, 0x52, 0x53, 0x48, 0x52, 0x55, 0x42, 0x10, 0xd0, 0x10, 0x12, 0x0b,
	0x0a, 0x06, 0x47, 0x50, 0x42, 0x52, 0x55, 0x42, 0x10, 0xd1, 0x10, 0x12, 0x0c, 0x0a, 0x07, 0x50,
	0x4f, 0x53, 0x54, 0x52, 0x55, 0x42, 0x10, 0xd2, 0x10, 0x12, 0x0a, 0x0a, 0x05, 0x59, 0x4d, 0x52,
	0x55, 0x42, 0x10, 0xd4, 0x10, 0x12, 0x0b, 0x0a, 0x06, 0x53, 0x42, 0x50, 0x52, 0x55, 0x42, 0x10,
	0xd5, 0x10, 0x42, 0x1b, 0x5a, 0x19, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x67, 0x6f, 0x6c,
	0x61, 0x6e, 0x67, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x70, 0x2d, 0x63, 0x61, 0x63, 0x68, 0x65, 0x62,
	0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_enums_proto_rawDescOnce sync.Once
	file_enums_proto_rawDescData = file_enums_proto_rawDesc
)

func file_enums_proto_rawDescGZIP() []byte {
	file_enums_proto_rawDescOnce.Do(func() {
		file_enums_proto_rawDescData = protoimpl.X.CompressGZIP(file_enums_proto_rawDescData)
	})
	return file_enums_proto_rawDescData
}

var file_enums_proto_enumTypes = make([]protoimpl.EnumInfo, 3)
var file_enums_proto_goTypes = []interface{}{
	(Status)(0), // 0: p_cache.Status
	(Token)(0),  // 1: p_cache.Token
	(Bank)(0),   // 2: p_cache.Bank
}
var file_enums_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_enums_proto_init() }
func file_enums_proto_init() {
	if File_enums_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_enums_proto_rawDesc,
			NumEnums:      3,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_enums_proto_goTypes,
		DependencyIndexes: file_enums_proto_depIdxs,
		EnumInfos:         file_enums_proto_enumTypes,
	}.Build()
	File_enums_proto = out.File
	file_enums_proto_rawDesc = nil
	file_enums_proto_goTypes = nil
	file_enums_proto_depIdxs = nil
}
