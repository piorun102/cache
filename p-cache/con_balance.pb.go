// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.32.0
// 	protoc        v4.25.1
// source: controller/con_balance.proto

package p_cache

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	_ "google.golang.org/protobuf/types/descriptorpb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CON_Balance struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Base   *Base   `protobuf:"bytes,1,opt,name=Base,proto3" json:"Base,omitempty"`
	Rate   float32 `protobuf:"fixed32,2,opt,name=Rate,proto3" json:"Rate,omitempty"`
	Before float32 `protobuf:"fixed32,3,opt,name=Before,proto3" json:"Before,omitempty"`
	After  float32 `protobuf:"fixed32,4,opt,name=After,proto3" json:"After,omitempty"`
}

func (x *CON_Balance) Reset() {
	*x = CON_Balance{}
	if protoimpl.UnsafeEnabled {
		mi := &file_controller_con_balance_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CON_Balance) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CON_Balance) ProtoMessage() {}

func (x *CON_Balance) ProtoReflect() protoreflect.Message {
	mi := &file_controller_con_balance_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CON_Balance.ProtoReflect.Descriptor instead.
func (*CON_Balance) Descriptor() ([]byte, []int) {
	return file_controller_con_balance_proto_rawDescGZIP(), []int{0}
}

func (x *CON_Balance) GetBase() *Base {
	if x != nil {
		return x.Base
	}
	return nil
}

func (x *CON_Balance) GetRate() float32 {
	if x != nil {
		return x.Rate
	}
	return 0
}

func (x *CON_Balance) GetBefore() float32 {
	if x != nil {
		return x.Before
	}
	return 0
}

func (x *CON_Balance) GetAfter() float32 {
	if x != nil {
		return x.After
	}
	return 0
}

type CON_BalanceList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Value []*CON_Balance `protobuf:"bytes,1,rep,name=Value,proto3" json:"Value,omitempty"`
}

func (x *CON_BalanceList) Reset() {
	*x = CON_BalanceList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_controller_con_balance_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CON_BalanceList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CON_BalanceList) ProtoMessage() {}

func (x *CON_BalanceList) ProtoReflect() protoreflect.Message {
	mi := &file_controller_con_balance_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CON_BalanceList.ProtoReflect.Descriptor instead.
func (*CON_BalanceList) Descriptor() ([]byte, []int) {
	return file_controller_con_balance_proto_rawDescGZIP(), []int{1}
}

func (x *CON_BalanceList) GetValue() []*CON_Balance {
	if x != nil {
		return x.Value
	}
	return nil
}

type CON_BalanceMap struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Value map[string]*CON_BalanceList `protobuf:"bytes,1,rep,name=value,proto3" json:"value,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
}

func (x *CON_BalanceMap) Reset() {
	*x = CON_BalanceMap{}
	if protoimpl.UnsafeEnabled {
		mi := &file_controller_con_balance_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CON_BalanceMap) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CON_BalanceMap) ProtoMessage() {}

func (x *CON_BalanceMap) ProtoReflect() protoreflect.Message {
	mi := &file_controller_con_balance_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CON_BalanceMap.ProtoReflect.Descriptor instead.
func (*CON_BalanceMap) Descriptor() ([]byte, []int) {
	return file_controller_con_balance_proto_rawDescGZIP(), []int{2}
}

func (x *CON_BalanceMap) GetValue() map[string]*CON_BalanceList {
	if x != nil {
		return x.Value
	}
	return nil
}

var File_controller_con_balance_proto protoreflect.FileDescriptor

var file_controller_con_balance_proto_rawDesc = []byte{
	0x0a, 0x1c, 0x63, 0x6f, 0x6e, 0x74, 0x72, 0x6f, 0x6c, 0x6c, 0x65, 0x72, 0x2f, 0x63, 0x6f, 0x6e,
	0x5f, 0x62, 0x61, 0x6c, 0x61, 0x6e, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07,
	0x70, 0x5f, 0x63, 0x61, 0x63, 0x68, 0x65, 0x1a, 0x20, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70,
	0x74, 0x6f, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0a, 0x63, 0x6f, 0x72, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x72, 0x0a, 0x0b, 0x43, 0x4f, 0x4e, 0x5f, 0x42, 0x61, 0x6c,
	0x61, 0x6e, 0x63, 0x65, 0x12, 0x21, 0x0a, 0x04, 0x42, 0x61, 0x73, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x0d, 0x2e, 0x70, 0x5f, 0x63, 0x61, 0x63, 0x68, 0x65, 0x2e, 0x42, 0x61, 0x73,
	0x65, 0x52, 0x04, 0x42, 0x61, 0x73, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x52, 0x61, 0x74, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x02, 0x52, 0x04, 0x52, 0x61, 0x74, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x42,
	0x65, 0x66, 0x6f, 0x72, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x02, 0x52, 0x06, 0x42, 0x65, 0x66,
	0x6f, 0x72, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x41, 0x66, 0x74, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01,
	0x28, 0x02, 0x52, 0x05, 0x41, 0x66, 0x74, 0x65, 0x72, 0x22, 0x3d, 0x0a, 0x0f, 0x43, 0x4f, 0x4e,
	0x5f, 0x42, 0x61, 0x6c, 0x61, 0x6e, 0x63, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x2a, 0x0a, 0x05,
	0x56, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x70, 0x5f,
	0x63, 0x61, 0x63, 0x68, 0x65, 0x2e, 0x43, 0x4f, 0x4e, 0x5f, 0x42, 0x61, 0x6c, 0x61, 0x6e, 0x63,
	0x65, 0x52, 0x05, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x9e, 0x01, 0x0a, 0x0e, 0x43, 0x4f, 0x4e,
	0x5f, 0x42, 0x61, 0x6c, 0x61, 0x6e, 0x63, 0x65, 0x4d, 0x61, 0x70, 0x12, 0x38, 0x0a, 0x05, 0x76,
	0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x70, 0x5f, 0x63,
	0x61, 0x63, 0x68, 0x65, 0x2e, 0x43, 0x4f, 0x4e, 0x5f, 0x42, 0x61, 0x6c, 0x61, 0x6e, 0x63, 0x65,
	0x4d, 0x61, 0x70, 0x2e, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x1a, 0x52, 0x0a, 0x0a, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x45, 0x6e,
	0x74, 0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x2e, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x18, 0x2e, 0x70, 0x5f, 0x63, 0x61, 0x63, 0x68, 0x65, 0x2e, 0x43,
	0x4f, 0x4e, 0x5f, 0x42, 0x61, 0x6c, 0x61, 0x6e, 0x63, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x42, 0x1b, 0x5a, 0x19, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x67, 0x6f, 0x6c, 0x61, 0x6e, 0x67, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x70,
	0x2d, 0x63, 0x61, 0x63, 0x68, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_controller_con_balance_proto_rawDescOnce sync.Once
	file_controller_con_balance_proto_rawDescData = file_controller_con_balance_proto_rawDesc
)

func file_controller_con_balance_proto_rawDescGZIP() []byte {
	file_controller_con_balance_proto_rawDescOnce.Do(func() {
		file_controller_con_balance_proto_rawDescData = protoimpl.X.CompressGZIP(file_controller_con_balance_proto_rawDescData)
	})
	return file_controller_con_balance_proto_rawDescData
}

var file_controller_con_balance_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_controller_con_balance_proto_goTypes = []interface{}{
	(*CON_Balance)(nil),     // 0: p_cache.CON_Balance
	(*CON_BalanceList)(nil), // 1: p_cache.CON_BalanceList
	(*CON_BalanceMap)(nil),  // 2: p_cache.CON_BalanceMap
	nil,                     // 3: p_cache.CON_BalanceMap.ValueEntry
	(*Base)(nil),            // 4: p_cache.Base
}
var file_controller_con_balance_proto_depIdxs = []int32{
	4, // 0: p_cache.CON_Balance.Base:type_name -> p_cache.Base
	0, // 1: p_cache.CON_BalanceList.Value:type_name -> p_cache.CON_Balance
	3, // 2: p_cache.CON_BalanceMap.value:type_name -> p_cache.CON_BalanceMap.ValueEntry
	1, // 3: p_cache.CON_BalanceMap.ValueEntry.value:type_name -> p_cache.CON_BalanceList
	4, // [4:4] is the sub-list for method output_type
	4, // [4:4] is the sub-list for method input_type
	4, // [4:4] is the sub-list for extension type_name
	4, // [4:4] is the sub-list for extension extendee
	0, // [0:4] is the sub-list for field type_name
}

func init() { file_controller_con_balance_proto_init() }
func file_controller_con_balance_proto_init() {
	if File_controller_con_balance_proto != nil {
		return
	}
	file_core_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_controller_con_balance_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CON_Balance); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_controller_con_balance_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CON_BalanceList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_controller_con_balance_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CON_BalanceMap); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_controller_con_balance_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_controller_con_balance_proto_goTypes,
		DependencyIndexes: file_controller_con_balance_proto_depIdxs,
		MessageInfos:      file_controller_con_balance_proto_msgTypes,
	}.Build()
	File_controller_con_balance_proto = out.File
	file_controller_con_balance_proto_rawDesc = nil
	file_controller_con_balance_proto_goTypes = nil
	file_controller_con_balance_proto_depIdxs = nil
}
