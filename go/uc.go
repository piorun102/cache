package cache

import (
	p_cache "gitlab.com/piorun102/cache/p-cache"
	"gitlab.com/piorun102/lg"
	bwgNats "gitlab.com/piorun102/pkg/storage/nats"
)

type UC struct {
	nc bwgNats.NC
}

func New(nc bwgNats.NC) *UC {
	return &UC{
		nc: nc,
	}
}

const CorIDs = "CorIDs"

func (c *UC) InsertCorIDs(ctx lg.CtxLogger, input *p_cache.COR_IDList) error {
	return c.nc.InsertCache(ctx, CorIDs, input)
}
func (c *UC) SelectCorIDs(ctx lg.CtxLogger) (res []string, err error) {
	var output = &p_cache.COR_IDList{}
	err = c.nc.SelectCache(ctx, CorIDs, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make([]string, 0)
		return
	}
	res = output.GetValue()
	return
}
