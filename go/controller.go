package cache

import (
	p_cache "gitlab.com/piorun102/cache/p-cache"
	"gitlab.com/piorun102/lg"
)

const ConBalance = "ConBalance"

func (c *UC) InsertConBalance(ctx lg.CtxLogger, input *p_cache.CON_BalanceMap) error {
	return c.nc.InsertCache(ctx, ConBalance, input)
}
func (c *UC) SelectConBalance(ctx lg.CtxLogger) (res map[string]*p_cache.CON_BalanceList, err error) {
	var output = &p_cache.CON_BalanceMap{}
	err = c.nc.SelectCache(ctx, ConBalance, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make(map[string]*p_cache.CON_BalanceList, 0)
		return
	}
	res = output.GetValue()
	return
}

const ConTransaction = "ConTransaction"

func (c *UC) InsertConTransaction(ctx lg.CtxLogger, input *p_cache.CON_TxMap) error {
	return c.nc.InsertCache(ctx, ConTransaction, input)
}
func (c *UC) SelectConTransaction(ctx lg.CtxLogger) (res map[string]*p_cache.CON_Tx, err error) {
	var output = &p_cache.CON_TxMap{}
	err = c.nc.SelectCache(ctx, ConTransaction, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make(map[string]*p_cache.CON_Tx, 0)
		return
	}
	res = output.GetValue()
	return
}

const ConServiceData = "ConServiceData"

func (c *UC) InsertConServiceData(ctx lg.CtxLogger, input *p_cache.CON_ServiceDataList) error {
	return c.nc.InsertCache(ctx, ConServiceData, input)
}
func (c *UC) SelectConServiceData(ctx lg.CtxLogger) (res []*p_cache.CON_ServiceData, err error) {
	var output = &p_cache.CON_ServiceDataList{}
	err = c.nc.SelectCache(ctx, ConServiceData, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make([]*p_cache.CON_ServiceData, 0)
		return
	}
	res = output.GetValue()
	return
}

const ConServiceName = "ConServiceName"

func (c *UC) InsertConServiceName(ctx lg.CtxLogger, input *p_cache.CON_ServiceName) error {
	return c.nc.InsertCache(ctx, ConServiceName, input)
}
func (c *UC) SelectConServiceName(ctx lg.CtxLogger) (res map[int64]string, err error) {
	var output = &p_cache.CON_ServiceName{}
	err = c.nc.SelectCache(ctx, ConServiceName, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make(map[int64]string, 0)
		return
	}
	res = output.GetValue()
	return
}
