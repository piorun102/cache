package cache

import (
	"github.com/pkg/errors"
	p_cache "gitlab.com/piorun102/cache/p-cache"
	"gitlab.com/piorun102/lg"
)

const MarOffer = "MarOffer"

func (c *UC) InsertMarOffer(ctx lg.CtxLogger, input *p_cache.MAR_OfferMap) error {
	return c.nc.InsertCache(ctx, MarOffer, input)
}
func (c *UC) SelectMarOffer(ctx lg.CtxLogger) (res map[string]*p_cache.MAR_Offer, err error) {
	var output = &p_cache.MAR_OfferMap{}
	err = c.nc.SelectCache(ctx, MarOffer, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make(map[string]*p_cache.MAR_Offer, 0)
		return
	}
	res = output.GetValue()
	return
}

const MarOrder = "MarOrder"

func (c *UC) InsertMarOrder(ctx lg.CtxLogger, input *p_cache.MAR_OrderMap) error {
	return c.nc.InsertCache(ctx, MarOrder, input)
}
func (c *UC) SelectMarOrder(ctx lg.CtxLogger) (res map[string]*p_cache.MAR_OrderList, err error) {
	var output = &p_cache.MAR_OrderMap{}
	err = c.nc.SelectCache(ctx, MarOrder, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make(map[string]*p_cache.MAR_OrderList, 0)
		return
	}
	res = output.GetValue()
	return
}

const MarToken = "MarToken"

func (c *UC) InsertMarToken(ctx lg.CtxLogger, input *p_cache.MAR_TokenList) error {
	return c.nc.InsertCache(ctx, MarToken, input)
}
func (c *UC) SelectMarToken(ctx lg.CtxLogger) (res []*p_cache.MAR_Token, err error) {
	var output = &p_cache.MAR_TokenList{}
	err = c.nc.SelectCache(ctx, MarToken, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		err = errors.New("nil value")
		return
	}
	res = output.GetValue()
	return
}

const MarSwapPrice = "MarSwapPrice"

func (c *UC) InsertMarSwapPrice(ctx lg.CtxLogger, input *p_cache.MAR_SwapPriceMap) error {
	return c.nc.InsertCache(ctx, MarSwapPrice, input)
}
func (c *UC) SelectMarSwapPrice(ctx lg.CtxLogger) (res map[int32]*p_cache.MAR_SwapPrice, err error) {
	var output = &p_cache.MAR_SwapPriceMap{}
	err = c.nc.SelectCache(ctx, MarSwapPrice, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		err = errors.New("nil value")
		return
	}
	res = output.GetValue()
	return
}

const MarUserCryptoCommission = "MarUserCryptoCommission"

func (c *UC) InsertMarUserCryptoCommission(ctx lg.CtxLogger, input *p_cache.MAR_UserCryptoCommissionMap) error {
	return c.nc.InsertCache(ctx, MarUserCryptoCommission, input)
}
func (c *UC) SelectMarUserCryptoCommission(ctx lg.CtxLogger) (res map[int64]*p_cache.MAR_UserCryptoCommissionList, err error) {
	var output = &p_cache.MAR_UserCryptoCommissionMap{}
	err = c.nc.SelectCache(ctx, MarUserCryptoCommission, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		err = errors.New("nil value")
		return
	}
	res = output.GetValue()
	return
}
