package cache

import (
	p_cache "gitlab.com/piorun102/cache/p-cache"
	"gitlab.com/piorun102/lg"
)

const StoOffer = "StoOffer"

func (c *UC) InsertStoOffer(ctx lg.CtxLogger, input *p_cache.STO_OfferMap) error {
	return c.nc.InsertCache(ctx, StoOffer, input)
}
func (c *UC) SelectStoOffer(ctx lg.CtxLogger) (res map[string]*p_cache.STO_Offer, err error) {
	var output = &p_cache.STO_OfferMap{}
	err = c.nc.SelectCache(ctx, StoOffer, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make(map[string]*p_cache.STO_Offer, 0)
		return
	}
	res = output.GetValue()
	return
}

const StoOrder = "StoOrder"

func (c *UC) InsertStoOrder(ctx lg.CtxLogger, input *p_cache.STO_OrderMap) error {
	return c.nc.InsertCache(ctx, StoOrder, input)
}
func (c *UC) SelectStoOrder(ctx lg.CtxLogger) (res map[string]*p_cache.STO_OrderList, err error) {
	var output = &p_cache.STO_OrderMap{}
	err = c.nc.SelectCache(ctx, StoOrder, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make(map[string]*p_cache.STO_OrderList, 0)
		return
	}
	res = output.GetValue()
	return
}
