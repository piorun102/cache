package cache

import (
	p_cache "gitlab.com/piorun102/cache/p-cache"
	"gitlab.com/piorun102/lg"
)

const TraLog = "TraLog"

func (c *UC) InsertTraLog(ctx lg.CtxLogger, input *p_cache.TRA_LogMapMap) error {
	return c.nc.InsertCache(ctx, TraLog, input)
}
func (c *UC) SelectTraLog(ctx lg.CtxLogger) (res map[string]*p_cache.TRA_LogMap, err error) {
	var output = &p_cache.TRA_LogMapMap{}
	err = c.nc.SelectCache(ctx, TraLog, output)
	if err != nil {
		return
	}
	if output.GetValue() == nil {
		res = make(map[string]*p_cache.TRA_LogMap, 0)
		return
	}
	res = output.GetValue()
	return
}
